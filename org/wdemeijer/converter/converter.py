'''
Created on Aug 23, 2013

@author: wouter
'''
import sys
import csv

class PaymentEntry:
    def __init__(self, date, payee, inflow, outflow, memo):
        self.date = date
        self.payee = payee
        self.inflow = inflow
        self.outflow = outflow
        self.memo = memo
    def get_formatted_data(self):
        return [self.date, self.payee, self.inflow, self.outflow, self.memo]
    def isOutflow(self):
        inflow = float(self.inflow.replace(",","."))
        outflow = float(self.outflow.replace(",", "."))
        
        if(inflow > outflow):
            return False
        else:
            return True

def main(argv):
    ing_csv_location = argv[0]
    ynab_csv_location = argv[1]
    
    columnMap = {"date":    0,
                 "payee":   1,
                 "deporext":5,
                 "amount":  6,
                 "memo":    8
                 }
    
    
    print ing_csv_location;
    paymentEntries = set([])
    
    with open(ing_csv_location, 'rb') as ingcsvfile:
        
        with open(ynab_csv_location, 'wb') as csvfile:
            ynabcsvfile = csv.writer(csvfile, delimiter=',',
                                        quotechar='"', quoting=csv.QUOTE_MINIMAL)
            ynabcsvfile.writerow(["Date","Payee","Inflow","Outflow","Memo"])
            ingreader = csv.reader(ingcsvfile, delimiter=',', quotechar='"')
            ingreader.next(); #skip header row
            for row in ingreader:
                raw_amount = row[columnMap.get('amount')]
                
                if row[columnMap.get('deporext')] == "Bij":
                    inflow = raw_amount
                    outflow = "0"
                else:
                    inflow = "0"
                    outflow = raw_amount
                paymentEntry = PaymentEntry(row[columnMap.get('date')],
                                            row[columnMap.get('payee')],
                                            inflow,
                                            outflow,
                                            row[columnMap.get('memo')])
                ##if(paymentEntry.isOutflow()):
               	print paymentEntry.get_formatted_data()
                ynabcsvfile.writerow(paymentEntry.get_formatted_data())
                
main(sys.argv[1:])